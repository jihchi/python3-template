# Python3 Cookiecutter Template

### Usage

The recommended [Python](https://python.org/) version is 3.6 or later.

Install [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) and create a project from the template:

```sh
$ pip install cookiecutter
$ git clone https://gitlab.com/pienaarp/python3-template.git
$ cookiecutter python3-template

project_name []: Generated Project
project_slug [generated_project]: 
package_name [generated_project]: 
project_description []: My new Python3 project
Select project_version:
1 - 0.1.0
2 - 1.0.0
Choose from 1, 2 (1, 2) [1]: 1
...
...
```

When the project has been generated Cookiecutter will:
  - Create a virtual environment
  - Install dependencies from requirements.txt
  - Initialize a Git repository
  - Add a pre-commit hook to run **make**
  
Once generated, activate the virtual environment:

```sh
$ cd generated_project
$ . venv/bin/activate
```


### Toolchain

**Makefile** defines the toolchain:
  - formatting - [black](https://github.com/ambv/black)
  - linting - [pylint](https://www.pylint.org/)
  - type checking - [pylint](http://mypy-lang.org/)
  - testing - [pytest](https://docs.pytest.org/en/latest/)
  - packaging - [setuptools](https://setuptools.readthedocs.io/en/latest/)

Run **make** to build the project:

```sh
$ make
```

**make** will also run before committing, thanks to the pre-commit hook.

To disable this commit with the --no-verify flag:

```sh
$ git commit --no-verify -m "commit message"
$ git commit -n -m "commit message"
```

Or remove **.git/hooks/pre-commit** to disable permanently.

<EOM>