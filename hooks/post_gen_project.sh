#!/bin/bash
export PYTHONPATH=.

python3 -V 
python3 -m venv venv
. venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt

git init
git add --all
git commit -m "Initial commit"

echo make > .git/hooks/pre-commit
chmod 755 .git/hooks/pre-commit


