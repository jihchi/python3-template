import setuptools

setuptools.setup(
    name="{{ cookiecutter.project_slug }}",
    version="{{ cookiecutter.project_version }}",
    description="{{ cookiecutter.project_description }}",
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'})
